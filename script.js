const apiKey = "bb950a668ff9aa871c432e2b94bca17e";
const apiUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&q=";
const searchBox = document.querySelector(".search input");
const searchBtn = document.querySelector(".search button");
const weatherIcon = document.querySelector(".weather-icon");
const mapWeather = [
    {
        name: "Clouds",
        image: "cloud.png"
    },
    {
        name: "Mist",
        image: "mist.png"
    },
    {
        name: "Snow",
        image: "snow.png"
    },
    {
        name: "Rain",
        image: "rain.png"
    },
    {
        name: "Drizzle",
        image: "drizzle.png"
    },
    {
        name: "Clear",
        image: "clear.png"
    },
]

const checkWeather = async (city="Da%20Nang") => {
    const response = await fetch(`${apiUrl}${city}&appid=${apiKey}`);
    if(response.status !== 200) {
        document.querySelector(".error").style.display = "block";
        document.querySelector(".weather").style.display = "none";
        return;
    }

    let data = await response.json();

    document.querySelector(".city").innerHTML = data.name;
    document.querySelector(".temp").innerHTML = Math.round(data.main.temp) + "°c";
    document.querySelector(".humidity").innerHTML = data.main.humidity + "%";
    document.querySelector(".wind").innerHTML = Math.round(data.wind.speed) + " km/h";
    const imagePath = mapWeather.find(x => x.name === data.weather[0].main);
    weatherIcon.src = `public/${imagePath.image}`;

    document.querySelector(".error").style.display = "none";
    document.querySelector(".weather").style.display = "block";
}

searchBtn.addEventListener("click",  async () => {
    await checkWeather(searchBox.value);
})

checkWeather();